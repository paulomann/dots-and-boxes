﻿package  
{	
	import flash.events.*;
	import flash.net.navigateToURL;
	import flash.net.URLVariables;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.media.StageWebView;
	import flash.geom.Rectangle;
	public class FacebookController 
	{
		private var facebookConnect:FacebookConnect;
		private var engine;
		private var singleton:Singleton;
		private var _stage;
		private var _webView:StageWebView;
        private var _topStripe;
		private var shareScreen;
		private var name = Constants.GAME_NAME;
		private var redirect_uri = 'https://google.com.br';
		
		public function FacebookController(engine:Engine) 
		{
			this.engine = engine;
			this._stage = engine.stage;
			singleton = Singleton.getInstance();
			facebookConnect = new FacebookConnect(engine.stage);
			facebookConnect.addEventListener(Constants.FACEBOOK_DISCONNECT, facebookDisconnect);
			facebookConnect.addEventListener(Constants.FACEBOOK_CONNECT, facebookConnectEvent);
			facebookConnect.addEventListener(Constants.CLOSE_WEB_VIEW, closeWebView);
		}
		public function facebookConnectEvent(e:Event) //Now i'm sure that he logged on facebook.
		{
			facebookConnect.addEventListener(Constants.FACEBOOK_DISCONNECT, facebookDisconnect);
			engine.menu.logoutFacebook.addEventListener(MouseEvent.MOUSE_DOWN, logoutFacebook);
			engine.menu.logoutFacebook.visible = true;
			engine.menu.loginFacebook.visible = false;
			singleton.loggedOnFacebook = true;
			engine.saveStatistics();
		}
		public function facebookDisconnect(e:Event)
		{
			facebookConnect.removeEventListener(Constants.FACEBOOK_DISCONNECT, facebookDisconnect);
			engine.menu.loginFacebook.addEventListener(MouseEvent.MOUSE_DOWN, loginFacebook);
			engine.menu.logoutFacebook.visible = false;
			engine.menu.loginFacebook.visible = true;
			singleton.loggedOnFacebook = false;
		}
		public function closeWebView(e:Event)
		{
			engine.menu.loginFacebook.addEventListener(MouseEvent.MOUSE_DOWN, loginFacebook);
			engine.menu.loginFacebook.visible = true;
			engine.menu.logoutFacebook.visible = false;
		}
		public function setupFacebookLoginButton()
		{
			if(engine != null)
			{
				if(singleton.loggedOnFacebook)
				{
					engine.menu.logoutFacebook.addEventListener(MouseEvent.MOUSE_DOWN, logoutFacebook);
					engine.menu.loginFacebook.visible = false;
				}
				else
				{
					engine.menu.loginFacebook.addEventListener(MouseEvent.MOUSE_DOWN, loginFacebook);
					engine.menu.logoutFacebook.visible = false;
				}
			}
		}
		public function logoutFacebook(e:Event)
		{
			engine.menu.logoutFacebook.removeEventListener(MouseEvent.MOUSE_DOWN, logoutFacebook);
			facebookConnect.logout();
		}
		public function loginFacebook(e:Event)
		{
			engine.menu.loginFacebook.removeEventListener(MouseEvent.MOUSE_DOWN, loginFacebook);
			facebookConnect.initLogin();
		}
		public function shareGame()
		{
			shareScreen = new ShareScreen();
			shareScreen.width = Constants.SCREEN_WIDTH;
			shareScreen.height = Constants.SCREEN_HEIGHT;
			engine.stage.addChild(shareScreen);
			shareScreen.facebookShare.addEventListener(MouseEvent.MOUSE_DOWN, shareOnFacebook);
			shareScreen.twitterShare.addEventListener(MouseEvent.MOUSE_DOWN, shareOnTwitter);
			shareScreen.bButton.addEventListener(MouseEvent.MOUSE_DOWN, backScreen);
		}
		function shareOnFacebook(e:Event)
		{
			var caption = 'Awesome ' + name + ' game!';
			setupStageWebView();
			var url:String = "https://www.facebook.com/dialog/feed?app_id=" + Constants.APP_ID + "&link=" + Constants.PLAY_STORE_URL + "&name=" + name +"&caption=" + caption +"&redirect_uri=" + redirect_uri;
   			_webView.loadURL(url);
		}
		function shareOnTwitter(e:Event)
		{
			var caption = 'Awesome ' + name +' game! Download on ';
			setupStageWebView();
			var url = "http://twitter.com/share?text=" + caption + "&url=" + Constants.PLAY_STORE_URL;
			trace(url);
			_webView.loadURL(url);
		}
		function setupStageWebView()
		{
			_topStripe = new WebViewCloseStripe();
			_topStripe.width = Constants.SCREEN_WIDTH;
			_topStripe.scaleY = _topStripe.scaleX;
			
            _topStripe.closeButton.addEventListener(MouseEvent.CLICK, closeWebview);
            _stage.addChild(_topStripe);
			
            _webView = new StageWebView();
			_webView.stage = _stage;
            _webView.viewPort = new Rectangle(0, _topStripe.height, Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT - _topStripe.height);
			_webView.assignFocus();
			_webView.addEventListener(LocationChangeEvent.LOCATION_CHANGE, successShare);
		}
		public function closeWebview(e:Event = null)
		{
			if(_topStripe != null)
			{
				_topStripe.closeButton.removeEventListener(MouseEvent.CLICK, closeWebview);
				_stage.removeChild(_topStripe);
			}
            _topStripe = null;
			if(_webView)
				_webView.dispose();
			_webView = null;
		}
		
		public function backScreen(e:Event = null)
		{
			if(shareScreen != null)
			{
				_stage.removeChild(shareScreen);
				shareScreen.facebookShare.removeEventListener(MouseEvent.MOUSE_DOWN, shareOnFacebook);
				shareScreen.twitterShare.removeEventListener(MouseEvent.MOUSE_DOWN, shareOnTwitter);
				shareScreen.bButton.removeEventListener(MouseEvent.MOUSE_DOWN, backScreen);
			}
			shareScreen = null;
		}
		public function successShare(e:LocationChangeEvent)
		{
			trace(e.toString());
			var obj:Object = e;
			if(obj.location.indexOf("https://www.google.com.br/?post_id") >= 0 
			   || obj.location.indexOf("/intent/tweet/complete?") >= 0)
		    {
			   singleton.sharedOnSocialNetwork = true;
			   engine.saveStatistics();
			   closeWebview();
			   backScreen();
		    }
		}
	}
}
