﻿package  
{	
	//import com.appnext.appnextsdk.AppnextASExtension;
	/*import com.admob.*;*/
	import flash.net.SharedObject;
	import flash.geom.Rectangle;
	import flash.system.Capabilities;
	import flash.display.*;
	import flash.events.*;
	import flash.ui.Keyboard;
	import flash.desktop.NativeApplication;
	import Constants;
	import DotBoard;
	public class Engine extends MovieClip
	{
		private var dotBoard:DotBoard;
		public var menu:MenuScreen;
		private var difficultyScreen;
		private var ppi:Number;
		private var statisticsScreen:StatisticsScreen;
		private var singleton:Singleton;
		private var save:SharedObject;
		private var facebookConnect:FacebookConnect;
		private var facebookController:FacebookController;
		private var selectRaceScreen;
		private var selectCharScreen;
		private var player1;
		private var player2;
		/*private var adMobManager:AdMobManager;*/
		/*public static const banner_id:String="ca-app-pub-8984899036776833/8924264300";
		public static const full_id:String="ca-app-pub-8984899036776833/3237657502";*/
		public function Engine() 
		{
			setupScreen();
			init();
			setupInterstitial();
		}
		public function setupScreen()
		{
			stage.scaleMode = StageScaleMode.NO_SCALE;
			stage.align = StageAlign.TOP_LEFT;
			
			/*  ****IMPORTANT****
				Hardcoded size of screen here. 
			*/
			var guiSize:Rectangle = new Rectangle(0, 0, 480, 640);
			var deviceSize:Rectangle = new Rectangle(0, 0, stage.fullScreenWidth, stage.fullScreenHeight);
			var appScale:Number = 1;
			var appSize:Rectangle = guiSize.clone();
			// if device is wider than GUI's aspect ratio, height determines scale
			if ((deviceSize.width/deviceSize.height) > (guiSize.width/guiSize.height)) 
			{
				appScale = deviceSize.height / guiSize.height;
				appSize.width = deviceSize.width / appScale;
			} 
			// if device is taller than GUI's aspect ratio, width determines scale
			else 
			{
				appScale = deviceSize.width / guiSize.width;
				appSize.height = deviceSize.height / appScale;
			}
			stage.stageWidth = appSize.width;
        	stage.stageHeight = appSize.height;
		}
		public function init()
		{
			singleton = Singleton.getInstance();
			singleton.soundController = new SoundController();
			save = SharedObject.getLocal("game_statistics");
			initializeData();
			difficultyScreen = new DifficultyScreen();
			setupConstants();
			setupMenu();
			facebookController = new FacebookController(this);
			facebookController.setupFacebookLoginButton();
		}
		public function setupInterstitial()
		{
			/*var appnextExtension:AppnextASExtension = null;
			appnextExtension = new AppnextASExtension();
			appnextExtension.init("a10ff183-6685-4b62-999f-b95eaa7bafa2");// Set your AppID
			appnextExtension.showPopup("a10ff183-6685-4b62-999f-b95eaa7bafa2"); // show the interstitial*/
			/*adMobManager = AdMobManager.manager;
			if(adMobManager.isSupported)
			{
				adMobManager.verbose = true;
				adMobManager.operationMode = AdMobManager.TEST_MODE;
				
				// Set AdMobId settings
				if (adMobManager.device == AdMobManager.IOS){
					adMobManager.bannersAdMobId =banner_id;// "ADMOB_IOS_BANNER_ID";
				}else{
					adMobManager.bannersAdMobId =full_id;// "ADMOB_ANDROID_BANNER_ID";
				}
				
				// Set Targetting Settings [Optional]
				adMobManager.gender = AdMobManager.GENDER_MALE;
				adMobManager.birthYear = 1996;
				adMobManager.birthMonth = 1;
				adMobManager.birthDay = 24;
				adMobManager.isCDT = true;
				
				// Create The Banner
				adMobManager.addEventListener(AdMobEvent.BANNER_LOADED, onBannerLoaded);
				adMobManager.createBanner(AdMobSize.BANNER,AdMobPosition.MIDDLE_CENTER,"BottomBanner", null, true);
			}*/
		}
		/*public function onBannerLoaded(e:AdMobEvent):void
		{
			// Show the specific banner who did dispatch the event
			adMobManager.showBanner(e.data);
		}*/
		public function initializeData()
		{
			if(save.data.statistics != null)
			{
				singleton.wins = save.data.statistics.wins;
				singleton.losses = save.data.statistics.losses;
				singleton.squaresClosed = save.data.statistics.squaresClosed;
				singleton.loggedOnFacebook = save.data.statistics.loggedOnFacebook;
				singleton.sharedOnSocialNetwork = save.data.statistics.sharedOnSocialNetwork;
			}
		}
		public function setupMenu()
		{
			menu = new MenuScreen();
			addEventListeners();
			menu.width = Constants.SCREEN_WIDTH;
			menu.height = Constants.SCREEN_HEIGHT;
			singleton.soundController.playMenuSound();
			stage.addChild(menu);
		}
		public function eventHandler(e:Event)
		{
			singleton.soundController.playClickSound();
			/* it could be useful here :  var child:DisplayObject = stage.getChildByName("btnPortable"); */
			var string:String = trim(e.target.text);
			/* Player 1 is always true */
			var color:Boolean = true;
			stage.removeChild(menu);
			removeListeners();
			switch(string)
			{
				case "One Player":
					resetGameType()
					singleton.onePlayer = true;
					difficultyScreen.width = Constants.SCREEN_WIDTH;
					difficultyScreen.height = Constants.SCREEN_HEIGHT;
					difficultyScreen.bButton.addEventListener(MouseEvent.MOUSE_DOWN, backToMenu);
					difficultyScreen.easy.addEventListener(MouseEvent.MOUSE_DOWN, easy);
					difficultyScreen.medium.addEventListener(MouseEvent.MOUSE_DOWN, medium);
					difficultyScreen.expert.addEventListener(MouseEvent.MOUSE_DOWN, expert);
					stage.addChild(difficultyScreen);
					break;
				case "Two Players":
					resetGameType()
					singleton.twoPlayer = true;
					setupChooseBoardSizeScreen();
					break;
				case "AI vs AI":
					resetGameType()
					singleton.AIGame = true;
					setupChooseBoardSizeScreen();
					break;
				case "Exit":
					NativeApplication.nativeApplication.exit(); 
					break;
				case "Statistics":
					resetGameType()
					statisticsScreen = new StatisticsScreen();
					statisticsScreen.wins.winsNumber.text = singleton.wins;
					statisticsScreen.losses.lossesNumber.text = singleton.losses;
					statisticsScreen.squares.squaresNumber.text = singleton.squaresClosed;
					statisticsScreen.winrate.winrateNumber.text = int(100*(singleton.wins/(singleton.wins + singleton.losses))) + '%';
					statisticsScreen.bButton.addEventListener(MouseEvent.MOUSE_DOWN, backToMenu);
					statisticsScreen.width = Constants.SCREEN_WIDTH;
					statisticsScreen.height = Constants.SCREEN_HEIGHT;
					stage.addChild(statisticsScreen);
					break;
			}
		}
		function trim(s:String):String
		{
		  return s.replace( /^([\s|\t|\n]+)?(.*)([\s|\t|\n]+)?$/gm, "$2" );
		}
		public function setupConstants()
		{
			var serverString:String = unescape(Capabilities.serverString);
			ppi = Number(serverString.split("&DP=", 2)[1]);
			Constants.SCREEN_HEIGHT = stage.stageHeight;
			Constants.SCREEN_WIDTH = stage.stageWidth;
			Constants.DOT_SIZE = inchesToPixels(0.115);
			Constants.DOT_MAX_NEIGHBOURS = 4;
			Constants.FADE_IN_ANIMATION_TIME = 1.4;
			Constants.EDGE_WIDTH = (new GreenHorizontal()).width;
			Constants.EDGE_HEIGHT = (new GreenHorizontal()).height;
			Constants.NUMBER_OF_CHARS = 3;
			singleton.playerSelecting = 1;
		}
		public function removeListeners()
		{
			menu.statistics.removeEventListener(MouseEvent.MOUSE_DOWN, eventHandler);
			menu.exitGame.removeEventListener(MouseEvent.MOUSE_DOWN, eventHandler);
			menu.twoPlayerGame.removeEventListener(MouseEvent.MOUSE_DOWN, eventHandler);
			menu.onePlayerGame.removeEventListener(MouseEvent.MOUSE_DOWN, eventHandler);
			menu.AIGame.removeEventListener(MouseEvent.MOUSE_DOWN, eventHandler);
			difficultyScreen.medium.removeEventListener(MouseEvent.MOUSE_DOWN, medium);
			difficultyScreen.expert.removeEventListener(MouseEvent.MOUSE_DOWN, expert);
		}
		public function addEventListeners()
		{
			menu.statistics.addEventListener(MouseEvent.MOUSE_DOWN, eventHandler);
			menu.exitGame.addEventListener(MouseEvent.MOUSE_DOWN, eventHandler);
			menu.twoPlayerGame.addEventListener(MouseEvent.MOUSE_DOWN, eventHandler);
			menu.onePlayerGame.addEventListener(MouseEvent.MOUSE_DOWN, eventHandler);
			menu.AIGame.addEventListener(MouseEvent.MOUSE_DOWN, eventHandler);
		}
		public function easy(e:Event)
		{
			singleton.soundController.playClickSound();
			singleton.AIDifficulty = 1;//Constants.EASY);
			setupChooseBoardSizeScreen();
		}
		public function medium(e:Event)
		{
			singleton.soundController.playClickSound();
			singleton.AIDifficulty = 1;//Constants.MEDIUM);
			setupChooseBoardSizeScreen();
		}
		public function expert(e:Event)
		{
			singleton.soundController.playClickSound();
			singleton.AIDifficulty = 1;//Constants.HARD);
			setupChooseBoardSizeScreen();
		}
		public function setupChooseBoardSizeScreen()
		{
			var boardSizeScreen = new BoardSizeScreen();
			boardSizeScreen.width = Constants.SCREEN_WIDTH;
			boardSizeScreen.height = Constants.SCREEN_HEIGHT;
			boardSizeScreen.x4.addEventListener(MouseEvent.MOUSE_DOWN, x4);
			boardSizeScreen.x5.addEventListener(MouseEvent.MOUSE_DOWN, x5);
			boardSizeScreen.x6.addEventListener(MouseEvent.MOUSE_DOWN, x6);
			boardSizeScreen.bButton.addEventListener(MouseEvent.MOUSE_DOWN, removeScreen);
			stage.addChild(boardSizeScreen);
		}
		public function removeScreen(e:Event)
		{
			singleton.soundController.playClickSound();
			stage.removeChild(e.currentTarget.parent);
			if(!stage.contains(difficultyScreen))
			{
				addEventListeners();
				stage.addChild(menu);
				resetGameType()
			}
		}
		public function x4(e:Event)
		{
			singleton.soundController.playClickSound();
			Constants.NUMBER_OF_DOTS = 4;
			stage.removeChild(e.currentTarget.parent);
			selectRace();
			//startGame()
		}
		public function x5(e:Event)
		{
			singleton.soundController.playClickSound();
			if(singleton.sharedOnSocialNetwork)
			{
				Constants.NUMBER_OF_DOTS = 5;
				stage.removeChild(e.currentTarget.parent);
				selectRace();
				//startGame()
			}
			else
			{
				facebookController.shareGame();
			}
		}
		public function x6(e:Event)
		{
			singleton.soundController.playClickSound();
			if(singleton.sharedOnSocialNetwork)
			{
				Constants.NUMBER_OF_DOTS = 6;
				stage.removeChild(e.currentTarget.parent);
				selectRace();
				//startGame()
			}
			else
			{
				facebookController.shareGame();
			}
		}
		public function selectRace() //player variable will be used
		{
			if(!singleton.AIGame)
			{
				selectRaceScreen = new SelectRaceScreen();
				selectRaceScreen.width = Constants.SCREEN_WIDTH;
				selectRaceScreen.height = Constants.SCREEN_HEIGHT;
				selectRaceScreen.addEventListener(Constants.PIGS_SELECTED, pigsSelected);
				selectRaceScreen.addEventListener(Constants.HUMANS_SELECTED, humansSelected);
				stage.addChild(selectRaceScreen);
			}
			else
				startGame();
		}
		public function pigsSelected(e:Event)
		{
			stage.removeChild(selectRaceScreen);
			if(singleton.playerSelecting == 1)
			{
				player1 = new Player(!Constants.PIG);
				if(singleton.twoPlayer)
					player2 = new Player(!player1.getRace());
				startGame();
			}
		}
		public function humansSelected(e:Event)
		{
			stage.removeChild(selectRaceScreen);
			if(singleton.playerSelecting == 1)
			{
				player1 = new Player(Constants.PIG);
				if(singleton.twoPlayer)
					player2 = new Player(!player1.getRace());
				startGame();
			}
		}
		public function startGame()
		{
			singleton.playerSelecting = 1;
			var rand:int;
			if(singleton.AIGame)
			{
				rand = Math.random()*(Constants.NUMBER_OF_CHARS);
				player1 = new AI(Constants.PIG, 1);
				player1.setCharacter(rand);
				player2 = new AI(!Constants.PIG, 2);
				player2.setCharacter(rand);
			}
			else if(singleton.onePlayer)
			{
				rand = Math.random()*(Constants.NUMBER_OF_CHARS);
				stage.removeChild(difficultyScreen);
				player2 = new AI(!player1.getRace(), singleton.AIDifficulty);
				player2.setCharacter(rand);
			}
			player1.setPlayerColor(true); //It's the color associated 
			player2.setPlayerColor(false);//with the turn and the edge
			dotBoard = new DotBoard(player1, player2);
			dotBoard.addEventListener(Constants.GO_BACK_MENU_EVENT, removeGame);
			stage.addChild(dotBoard);
		}
		public function backToMenu(e:Event)
		{
			singleton.soundController.playClickSound();
			stage.removeChild(e.currentTarget.parent);
			addEventListeners();
			stage.addChild(menu);
		}
		public function removeGame(e:Event)
		{
			resetGameType()
			stage.removeChild(dotBoard);
			saveStatistics();
			addEventListeners();
			stage.addChild(menu);
		}
		public function saveStatistics()
		{
			var statistics:Object = {wins : singleton.wins,  losses : singleton.losses,
									squaresClosed : singleton.squaresClosed,
									loggedOnFacebook : singleton.loggedOnFacebook,
									sharedOnSocialNetwork : singleton.sharedOnSocialNetwork};
			save.data.statistics = statistics;
			save.flush();
		}
		private function inchesToPixels(inches:Number):uint
		{
		   return Math.round(ppi * inches);
		}
		public function resetGameType()
		{
			singleton.AIGame = false;
			singleton.twoPlayer = false;
			singleton.onePlayer = false;
		}
	}
}
