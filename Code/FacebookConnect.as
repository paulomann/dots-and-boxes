﻿package
{
	import Constants;
	import com.facebook.graph.FacebookMobile;
	import flash.geom.Rectangle;
	import flash.desktop.NativeApplication;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.system.Capabilities;
	import flash.display.DisplayObject
	import flash.display.Stage;
	import flash.events.*;
    import flash.events.Event;
    import flash.events.IOErrorEvent;
    import flash.external.ExternalInterface;
    import flash.net.URLLoader;
    import flash.net.URLRequest;
    import flash.system.Capabilities;
    import flash.system.Security;
    import flash.display.Loader;
	import flash.media.StageWebView;
	
	public class FacebookConnect extends Sprite
	{
		public static var APP_ID:String = "1595504897365860";
		private static const SITE_URL:String = "https://m.facebook.com/dialog/permissions.request?app_id=" + Constants.APP_ID + "&display=touch&next=http%3A%2F%2Fwww.facebook.com%2Fconnect%2Flogin_success.html&type=user_agent&fbconnect=1";
		private var _extendedPermissions:Array = ["user_friends", "user_website" ,"user_status" ,"user_about_me"];
		private var statusBar:Sprite;
		private var _webView:StageWebView;
        private var _topStripe;
        private var _activity:String;
        private var _timeoutID:uint;
		private var _stage:Stage;
        public static const ACTIVITY_LOGIN:String = "login";
        public static const ACTIVITY_POST:String = "post";
		public function FacebookConnect(stage:Stage)
		{
			this._stage = stage;
		}
		public function initLogin():void
		{
            _activity = ACTIVITY_LOGIN;
			FacebookMobile.init(Constants.APP_ID, onHandleInit);
        }
		public function onHandleInit(response:Object, fail:Object)
		{
			if (response)
			{
				dispatchEvent(new Event(Constants.FACEBOOK_CONNECT));
            }
            else
			{
                loginUser();
            }
		}
		private function loginUser():void 
		{
            _topStripe = new WebViewCloseStripe();
			_topStripe.width = Constants.SCREEN_WIDTH;
			_topStripe.scaleY = _topStripe.scaleX;
			
            _topStripe.closeButton.addEventListener(MouseEvent.CLICK, closeWebview);
            _stage.addChild(_topStripe);
			
            _webView = new StageWebView();
			_webView.stage = _stage;
            _webView.viewPort = new Rectangle(0, _topStripe.height, Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT - _topStripe.height);

            FacebookMobile.login(handleLogin, _stage, _extendedPermissions, _webView);
        }
		private function handleLogin(response:Object, fail:Object):void 
		{
            if(_topStripe) 
			{
                _topStripe.closeButton.removeEventListener(MouseEvent.CLICK, closeWebview);
                _stage.removeChild(_topStripe);
                _topStripe = null;
            }
            if(_webView) 
			{
                _webView = null;
            }
            if(response) 
			{
				//*****DISPATCH EVENT TO START GAME*****
                dispatchEvent(new Event(Constants.FACEBOOK_CONNECT));
            }
            else 
			{
				//LOGIN ERROR ESCREEN
				dispatchEvent(new Event(Constants.CLOSE_WEB_VIEW));
            }
        }
		private function closeWebview(e:MouseEvent):void 
		{
			_topStripe.closeButton.removeEventListener(MouseEvent.CLICK, closeWebview);
            _stage.removeChild(_topStripe);
            _topStripe = null;
			if(_webView)
			{
				_webView.dispose();
				_webView = null;
			}
			dispatchEvent(new Event(Constants.CLOSE_WEB_VIEW));
        }
		public function logout()
		{
			FacebookMobile.logout(handleReset, SITE_URL);
		}
		/* Its important to note that we may have a problem here, due to errors calling this funcion
		   TODO : Test it without internet connection.
		   DONE : Test done and there's no problem with a no internet connection*/
		public function handleReset(callback:Boolean)
		{
			dispatchEvent(new Event(Constants.FACEBOOK_DISCONNECT));
		}
		function trim(s:String):String
		{
		  return s.replace( /^([\s|\t|\n]+)?(.*)([\s|\t|\n]+)?$/gm, "$2" );
		}
	}
}