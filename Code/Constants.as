﻿package 
{
	public class Constants 
	{
		/* Do not change these values, they're all set in Engine class. */
		public static var SCREEN_WIDTH:int;
		public static var SCREEN_HEIGHT:int;
		public static var NUMBER_OF_DOTS:int;
		public static var DOT_SIZE:int;
		public static var DOT_MAX_NEIGHBOURS:int;
		public static var FADE_IN_ANIMATION_TIME;
		public static var NUMBER_OF_CHARS:int; // It's the number of pigs or the humans, it's the same number for both.
		/* This is set in setupDots() in DotBoard class */
		public static var DOT_DISTANCE:int;
		/* This edge width and height is the horizontal edge, the vertical one is just the inverse */
		public static var EDGE_WIDTH:int;
		public static var EDGE_HEIGHT:int;
		
		public static var ADS_HEIGHT:int = 0;
		public static var ADS_WIDTH:int = 0;
		
		public static var CHANGE_SCREEN_EVENT:String = "CHANGE_SCREEN_EVENT";
		public static var GO_BACK_MENU_EVENT:String = "GO_BACK_MENU_EVENT";
		public static var CONNECT_DOTS_EVENT:String = "CONNECT_DOTS_EVENT";
		public static var GAME_OVER_EVENT:String = "GAME_OVER_EVENT";
		
		/*Social network related*/
		public static var FACEBOOK_CONNECT:String = "FACEBOOK_CONNECT";
		public static var FACEBOOK_DISCONNECT:String = "FACEBOOK_DISCONNECT";
		public static var CLOSE_WEB_VIEW:String = "CLOSE_WEB_VIEW";
		public static var APP_ID:String = "1595504897365860";
		public static var PLAY_STORE_URL:String = "https://google.com.br";
		
		/*Assets controller Constants */
		public static var PIGS_SELECTED:String = "PIGS_SELECTED";
		public static var HUMANS_SELECTED:String = "HUMANS_SELECTED";
		public static var CHAR_SELECTED:String = "PIG_SELECTED";
		
		/*Initialized constants*/
		public static var EASY:int = 0;
		public static var MEDIUM:int = 1;
		public static var HARD:int = 2;
		public static var APP_SCHEME:String = "dotsandboxes";
		public static var GAME_NAME:String = "Dots and Boxes";
		public static var PIG:Boolean = true;
	}
}
