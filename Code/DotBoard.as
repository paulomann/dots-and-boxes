﻿package  
{	
	import flash.system.Capabilities;
	import flash.utils.Timer;
	import flash.geom.Rectangle;
	import flash.display.*;
	import flash.events.*;
	import fl.transitions.*;
 	import fl.transitions.easing.*;
	import flash.utils.getDefinitionByName;
	import Constants;
	import Engine;
	import Constants;
	public class DotBoard extends MovieClip
	{
		private var dots:Array;
		private var scorePane:ScorePane;
		private var player1:Player;
		private var player2:Player;
		private var geometricAssets:Array;
		private var bButton:BackButton;
		private var playerTurn:Player;
		private var ppi:Number;
		private var fakeEdges:Array;
		private var available:Boolean;
		private var singleton:Singleton;
		private var bgSquares:Array;
		public function DotBoard(player1:Player , player2:Player) 
		{
			/* The order here is important */
			
			singleton = Singleton.getInstance();
			singleton.soundController.silence();
			available = true;
			fakeEdges = new Array();
			geometricAssets = new Array();
			bgSquares = new Array();
			var serverString:String = unescape(Capabilities.serverString);
			ppi = Number(serverString.split("&DP=", 2)[1]);
			setupBackground();
			setupScorePane(player1, player2);
			resizeEdges();
			setupDots();
			setupPlayers(player1, player2, dots);
			setupBackButton();
			geometricAssets.push(bButton, scorePane);
			singleton.soundController.playGameSound();
			player1.canMove();
		}
		private function setupBackground()
		{
			var bg = new GameBackGround();
			bg.x = 0;
			bg.y = 0;
			bg.width = Constants.SCREEN_WIDTH;
			bg.height = Constants.SCREEN_HEIGHT;
			addChild(bg);
			geometricAssets.push(bg);
		}
		private function getPlayerCharAsset(player:Player):DisplayObject
		{
			var myClass:Class;
			var character = int(Math.random()*Constants.NUMBER_OF_CHARS);
			if(player.getRace() == Constants.PIG)
				myClass = getDefinitionByName("Pig" + character) as Class;
			else
				myClass = getDefinitionByName("Human" + character) as Class;
			var asset:Object = new myClass();
			return DisplayObject(asset);
		}
		private function setupScorePane(player1:Player, player2:Player)
		{
			scorePane = new ScorePane();
			scorePane.x = 0;
			scorePane.y = 0;
			var oldWidth = scorePane.width;
			var oldHeight = scorePane.height;
			scorePane.width = Constants.SCREEN_WIDTH;
			scorePane.scaleY = scorePane.scaleX;
			scorePane.player1.text = "0";
			scorePane.player2.text = "0";
			var widthDistance = scorePane.width - scorePane.player1.width - scorePane.player2.width;
			var minDistance =  widthDistance < scorePane.height ? widthDistance : scorePane.height; 
			minDistance = minDistance / scorePane.scaleX;

			var charPlayer1 = player1.getRace() == Constants.PIG ? new PigScore : new HumanScore;
			var charPlayer2 = player2.getRace() == Constants.PIG ? new PigScore : new HumanScore;
			player1.charAsset = charPlayer1;
			player2.charAsset = charPlayer2;
			
			charPlayer1.width = minDistance/2;
			charPlayer1.scaleY = charPlayer1.scaleX;
			charPlayer2.width = charPlayer1.width;
			charPlayer2.height = charPlayer1.height;
			
			charPlayer1.x = oldWidth / 2 - charPlayer1.width;
			charPlayer1.y = (oldHeight - charPlayer1.height)/2;
			charPlayer2.x = charPlayer1.x + charPlayer1.width;
			charPlayer2.y = charPlayer1.y;
			charPlayer2.gotoAndPlay(2);
			scorePane.addChild(charPlayer1);
			scorePane.addChild(charPlayer2);
			addChild(scorePane);
		}
		private function setupPlayers(player1:Player , player2:Player, dots:Array)
		{
			this.player1 = player1;
			this.player2 = player2;
			this.player1.addEventListener(Constants.CONNECT_DOTS_EVENT, connectDots);
			this.player1.addAdversary(player2);
			this.player2.addAdversary(player1);
			this.player1.init(dots);
			this.player2.init(dots);
			playerTurn = player1;
		}
		private function resizeEdges()
		{
			var proportion = Constants.EDGE_WIDTH / Constants.EDGE_HEIGHT;
			var minDistance = Constants.SCREEN_WIDTH < (Constants.SCREEN_HEIGHT - scorePane.height - Constants.ADS_HEIGHT) ? Constants.SCREEN_WIDTH : (Constants.SCREEN_HEIGHT - scorePane.height - Constants.ADS_HEIGHT);
			Constants.EDGE_WIDTH = (minDistance - ((Constants.NUMBER_OF_DOTS + 2)*Constants.DOT_SIZE))/(Constants.NUMBER_OF_DOTS - 1);
			Constants.EDGE_HEIGHT = Constants.EDGE_WIDTH / proportion;
		}
		private function setupDots()
		{
			dots = new Array(Constants.NUMBER_OF_DOTS);
			var space = Constants.EDGE_WIDTH + Constants.DOT_SIZE;
			Constants.DOT_DISTANCE = space;
			var spaceTop:int = scorePane.height;
			for(var i:int = 0; i < Constants.NUMBER_OF_DOTS; i++)
			{
				dots[i] = new Array(Constants.NUMBER_OF_DOTS);
				for(var j:int = 0; j < Constants.NUMBER_OF_DOTS; j++)
				{
					/* Dot is an object from points.fla's library. */
					var dot = new DotAsset();
					dot.width = Constants.DOT_SIZE;
					dot.height = Constants.DOT_SIZE;
					dots[i][j] = new Dot(i,j, dot, -1);
					dots[i][j].x = j*space + Constants.DOT_SIZE;
					dots[i][j].y = i*space + spaceTop + inchesToPixels(0.2);
					if(j != 0)
						setupEdge(dots[i][j-1], dots[i][j]);
					if(i != 0)
						setupEdge(dots[i-1][j], dots[i][j]);
					if(j != Constants.NUMBER_OF_DOTS - 1 && i != Constants.NUMBER_OF_DOTS - 1)
						createBackgroundSquare(dots[i][j]);
					dots[i][j].addEventListener(MouseEvent.MOUSE_DOWN, onClickDot);
					dots[i][j].addChildren();
					addChild(dots[i][j]);
					/* REMEMBER TO RELEASE ALL EVENTS */
				}
				this.addEventListener(MouseEvent.MOUSE_DOWN, onClickStage);
			}
		}
		public function onClickStage(e:Event)
		{
			this.removeEventListener(MouseEvent.MOUSE_DOWN, onClickStage);
			var x = stage.mouseX;
			var y = stage.mouseY;
			var minDist = bgSquares[0].width;
			var dot1:Dot, dot2:Dot;
			var l, j;
			var found = false;
			for(var i:int = 0; i < bgSquares.length; i++)
			{
				if(bgSquares[i].x < x && 
				   x < bgSquares[i].x + bgSquares[i].width &&
				   bgSquares[i].y < y &&
				   y < bgSquares[i].y + bgSquares[i].height)
				   {
					   l = Math.floor(i/(Constants.NUMBER_OF_DOTS-1));
					   j = i%(Constants.NUMBER_OF_DOTS-1);
					   if(x - bgSquares[i].x < minDist) //LEFT SIDE
					   {
						   minDist = x - bgSquares[i].x;
						   dot1 = dots[l][j];
						   dot2 = dots[l+1][j]
					   }
					   if(y - bgSquares[i].y < minDist) //TOP SIDE
					   {
						   minDist = y - bgSquares[i].y;
						   dot1 = dots[l][j];
						   dot2 = dots[l][j+1];
					   }
					   if(bgSquares[i].x + bgSquares[i].width - x < minDist) //RIGHT SIDE
					   {
						   minDist = bgSquares[i].x + bgSquares[i].width - x;
						   dot1 = dots[l][j+1];
						   dot2 = dots[l+1][j+1];
					   }
					   if(bgSquares[i].y + bgSquares[i].height - y < minDist) // DOWN SIDE
					   {
						   minDist = bgSquares[i].y + bgSquares[i].height - y;
						   dot1 = dots[l+1][j];
						   dot2 = dots[l+1][j+1];
					   }
					   found = true;
					   break;
				   }
			}
			if(found)
			{
				dot1.dispatchEvent(new MouseEvent(MouseEvent.MOUSE_DOWN));
				dot2.dispatchEvent(new MouseEvent(MouseEvent.MOUSE_DOWN));
			}
			this.addEventListener(MouseEvent.MOUSE_DOWN, onClickStage);
		}
		private function createBackgroundSquare(dot:Dot)
		{
			var square = new Sprite();
			square.graphics.beginFill(0x000000);
			square.graphics.drawRect(0, 0, Constants.EDGE_WIDTH, Constants.EDGE_WIDTH);
			square.x = dot.x + Constants.DOT_SIZE;
			square.y = dot.y + Constants.DOT_SIZE;
			bgSquares.push(square);
		}
		private function setupEdge(dot1:Dot, dot2:Dot)
		{
			var rec = new Sprite();
			fakeEdges.push(rec);
			rec.graphics.beginFill(0xFFFFFF);
			//horizontal
			if(dot1.y == dot2.y)
			{
				rec.x = dot1.x + Constants.DOT_SIZE;
				rec.y = dot1.y;
				rec.graphics.drawRect(0, 0, Constants.EDGE_WIDTH, Constants.DOT_SIZE);
			}
			else //vertical
			{
				rec.x = dot1.x;
				rec.y = dot1.y + Constants.DOT_SIZE;
				rec.graphics.drawRect(0, 0, Constants.DOT_SIZE, Constants.EDGE_WIDTH);
			}
			rec.graphics.endFill();
			rec.alpha = 0;
			rec.addEventListener(MouseEvent.MOUSE_DOWN, forceConnect);
			addChild(rec);
		}
		public function forceConnect(e:Event)
		{
			var target = e.currentTarget;
			var player = getPlayer();
			var j:int = ((target.x - Constants.DOT_SIZE)/Constants.DOT_DISTANCE);
			var i:int = ((target.y - inchesToPixels(0.2) - scorePane.height)/Constants.DOT_DISTANCE);
			if(target.width == Constants.DOT_SIZE)
			{
				dots[i][j].dispatchEvent(new MouseEvent(MouseEvent.MOUSE_DOWN));
				dots[i+1][j].dispatchEvent(new MouseEvent(MouseEvent.MOUSE_DOWN));
			}
			else
			{
				dots[i][j].dispatchEvent(new MouseEvent(MouseEvent.MOUSE_DOWN));
				dots[i][j+1].dispatchEvent(new MouseEvent(MouseEvent.MOUSE_DOWN));
			}
			e.currentTarget.removeEventListener(MouseEvent.MOUSE_DOWN, forceConnect);
		}
		private function setupBackButton()
		{
			
			bButton = new BackButton();
			var proportion = bButton.width / bButton.height;
			var spaceTop = dots[Constants.NUMBER_OF_DOTS - 1][Constants.NUMBER_OF_DOTS - 1].y;
			
			bButton.height = (Constants.SCREEN_HEIGHT - Constants.ADS_HEIGHT - spaceTop)/2;
			bButton.width = bButton.height * proportion;
			bButton.addEventListener(MouseEvent.MOUSE_DOWN, goBackToMenu);
			bButton.x = Constants.SCREEN_WIDTH/2 -  bButton.width/2;
			
			bButton.y =  spaceTop + bButton.height/2;
			addChild(bButton);
		}
		public function connectDots(e:Event)
		{
			var moveAgain:Boolean = false;
			var player:Player = Player(e.target);
			var adversary:Player = player.getAdversary();
			var clickedDots = player.getClickedDots();
			if(verifyAdjacency(clickedDots[0], clickedDots[1]) && !clickedDots[0].isConnectedToB(clickedDots[1]))
			{
				drawEdge(clickedDots[0], clickedDots[1], player);
				connect(clickedDots[0], clickedDots[1], player);
				removeListenerIfMaxNeighbours(clickedDots[0], clickedDots[1]);
				if(!drawSquareIfClosed(clickedDots[0], clickedDots[1], player))
					swapTurns(player, adversary);
				else
					moveAgain = true;
				if(gameOver())
				{
					gameOverScreen();
					return;
				}
			}
			refreshDotColors(clickedDots[0], clickedDots[1]);
			player.refreshDots();
			if(moveAgain)
				playerTurn = player;
			else
			{
				playerTurn = adversary;
			}
				
			/* Used to delay the AI */
			if(available)
			{
				var timer:Timer = new Timer(800, 0);
				timer.addEventListener(TimerEvent.TIMER, timerHandler);
				timer.start();
			}
		}
		public function timerHandler(e:TimerEvent)
		{
			e.target.removeEventListener(TimerEvent.TIMER, timerHandler);
			if(gameOver())
				gameOverScreen();
			else
				playerTurn.canMove();
		}
		public function onClickDot(e:Event = null)
		{
			var dot:Dot = Dot(e.currentTarget);
			var player:Player = getPlayer();
			//changeDotColor(dot);
			player.move_(dot);
		}
		public function getPlayer():Player
		{
			if(player1.hasEventListener(Constants.CONNECT_DOTS_EVENT))
				return player1;
			else
				return player1.getAdversary();
		}
		public function changeDotColor(dotNode:Dot)
		{
			dotNode.dot.gotoAndStop(2);
		}
		public function verifyAdjacency(dot1:Dot, dot2:Dot)
		{
			if(dot1.i - 1 == dot2.i && dot1.j == dot2.j)
				return true;
			else if(dot1.i == dot2.i && dot1.j + 1 == dot2.j)
				return true;
			else if(dot1.i + 1 == dot2.i && dot1.j == dot2.j)
				return true;
			else if(dot1.i == dot2.i && dot1.j - 1 == dot2.j)
				return true;
			return false;
		}
		public function drawEdge(dot1:Dot, dot2:Dot, player:Player)
		{
			var edge;
			var h_width = Constants.DOT_DISTANCE - Constants.DOT_SIZE;
			var h_height = Constants.DOT_SIZE + 10;
			var h = (Constants.DOT_DISTANCE + Constants.DOT_SIZE)/2 - (h_width > h_height ? h_width : h_height)/2;
			if(dot1.i == dot2.i)
			{
				edge = new PinkHorizontal();
				if(player.getPlayerColor())
					edge = new GreenHorizontal();
				edge.width = h_width;
				edge.height = h_height;
				edge.x = dot1.x < dot2.x ? dot1.x : dot2.x ;
				edge.x += h;
				edge.y = dot1.y - 4;
			}
			else
			{
				edge = new PinkVertical();
				if(player.getPlayerColor())
					edge = new GreenVertical();
				edge.width = h_height/2;
				edge.height = h_width;
				edge.x = dot1.x;
				edge.y = dot1.y < dot2.y ? dot1.y : dot2.y;
				edge.y += h;
			}
			addChild(edge);
			if(!available)
				setChildIndex(edge, this.numChildren-2);
			TransitionManager.start(edge, {type:Fade, direction:Transition.IN, duration:Constants.FADE_IN_ANIMATION_TIME, easing:Strong.easeOut});
			geometricAssets.push(edge);
			singleton.soundController.playFenceSound();
		}
		public function connect(dot1:Dot, dot2:Dot, player:Player)
		{
			dot1.addEdge(dot1, dot2, player.getPlayerColor());
			dot2.addEdge(dot2, dot1, player.getPlayerColor());
		}
		public function removeListenerIfMaxNeighbours(dot1:Dot, dot2:Dot)
		{
			if(dot1.getNumberOfNeighbours() == Constants.DOT_MAX_NEIGHBOURS)
				dot1.removeEventListener(MouseEvent.MOUSE_DOWN, onClickDot);
			if(dot2.getNumberOfNeighbours() == Constants.DOT_MAX_NEIGHBOURS)
				dot2.removeEventListener(MouseEvent.MOUSE_DOWN, onClickDot);
		}
		public function drawSquareIfClosed(dot1:Dot, dot2:Dot, player:Player):Boolean
		{
			var ret = false;
			if(dot1.i == dot2.i)
			{
				if(dot1.i != 0 && dot2.i != 0)
				{
					var top1, top2;
					top1 = dot1.isConnectedTo(dot1.i - 1, dot1.j);
					top2 = dot2.isConnectedTo(dot2.i - 1, dot2.j);
					
					if(top1 != null && top2 != null && top1.isConnectedToB(top2))
					{
					   drawSquare(top1.j > top2.j ? top2 : top1, player); 
					   ret = true;
					}
				}
				if(dot1.i != Constants.NUMBER_OF_DOTS - 1)
				{
					var down1, down2;
					down1 = dot1.isConnectedTo(dot1.i + 1, dot1.j);
					down2 = dot2.isConnectedTo(dot2.i + 1, dot2.j);
					
					if(down1 != null && down2 != null && down1.isConnectedToB(down2))
					{
					   drawSquare(dot1.j > dot2.j ? dot2 : dot1, player); 
					   ret = true;
					}
				}
			}
			else
			{
				if(dot1.j != 0 && dot2.j != 0)
				{
					var left1, left2;
					left1 = dot1.isConnectedTo(dot1.i, dot1.j - 1);
					left2 = dot2.isConnectedTo(dot2.i, dot2.j - 1);
					
					if(left1 != null && left2 != null && left1.isConnectedToB(left2))
					{
					   drawSquare(left1.i > left2.i ? left2 : left1, player); 
					   ret = true;
					}
				}
				if(dot1.j != Constants.NUMBER_OF_DOTS - 1)
				{
					var right1, right2;
					right1 = dot1.isConnectedTo(dot1.i, dot1.j + 1);
					right2 = dot2.isConnectedTo(dot2.i, dot2.j + 1);
					
					if(right1 != null && right2 != null && right1.isConnectedToB(right2))
					{
					   drawSquare(dot1.i > dot2.i ? dot2 : dot1, player); 
					   ret = true;
					}
				}
			}
			return ret;
		}
		public function drawSquare(originNode:Dot, player:Player)
		{
			if(player.getRace() == Constants.PIG)
				singleton.soundController.playManSound();
			else
				singleton.soundController.playPigSound();
			var square = getPlayerCharAsset(player);
			square.width = Constants.DOT_DISTANCE - Constants.DOT_SIZE;
			square.height = square.width;
			square.x = originNode.x + (Constants.DOT_DISTANCE + Constants.DOT_SIZE)/2 - square.width/2;
			square.y = originNode.y + (Constants.DOT_DISTANCE + Constants.DOT_SIZE)/2 - square.height/2;
			refreshScore(player);
			addChild(square);
			if(!available)
				setChildIndex(square, this.numChildren-2);
			TransitionManager.start(square, {type:Fade, direction:Transition.IN, duration:Constants.FADE_IN_ANIMATION_TIME, easing:Strong.easeOut});
			geometricAssets.push(square);
			singleton.squaresClosed++;
		}
		public function refreshScore(player:Player)
		{
			if(!player.getPlayerColor())
				scorePane.player1.text = String(parseInt(scorePane.player1.text) + 1);
			else
				scorePane.player2.text = String(parseInt(scorePane.player2.text) + 1);
		}
		public function swapTurns(player:Player, adversary:Player)
		{
			if(player.hasEventListener(Constants.CONNECT_DOTS_EVENT))
			{
				player.removeEventListener(Constants.CONNECT_DOTS_EVENT, connectDots);
				adversary.addEventListener(Constants.CONNECT_DOTS_EVENT, connectDots);
			}
			else
			{
				adversary.removeEventListener(Constants.CONNECT_DOTS_EVENT, connectDots);
				player.addEventListener(Constants.CONNECT_DOTS_EVENT, connectDots);
			}
			player1.charAsset.play();
			player2.charAsset.play();
			//scorePaneMessage();
		}
		public function gameOver()
		{
			var dots = this.dots;
			for(var i:int = 0; i < dots.length; i++)
			{
				for(var j:int = 0; j < dots[i].length; j++)
				{
					if((i == 0 && j == 0) ||
					   (i == Constants.NUMBER_OF_DOTS - 1 && j == Constants.NUMBER_OF_DOTS - 1) ||
					   (i == Constants.NUMBER_OF_DOTS - 1 && j == 0) ||
					   (i == 0 && j == Constants.NUMBER_OF_DOTS - 1))
				    {
					    if(dots[i][j].getNumberOfNeighbours() != 2)
					   	    return false;
				    }
					else if((i == 0 || i == Constants.NUMBER_OF_DOTS - 1)
					   || (j == 0 || j == Constants.NUMBER_OF_DOTS - 1))
					{
					 	if(dots[i][j].getNumberOfNeighbours() != 3)
							return false;
					}
					else if(dots[i][j].getNumberOfNeighbours() != 4)
						return false;
				}
			}
			return true;
		}
		public function gameOverScreen()
		{
			var player1Score = parseInt(scorePane.player1.text);
			var player2Score = parseInt(scorePane.player2.text);
			player1.charAsset.gotoAndStop(1);
			player2.charAsset.gotoAndStop(1);
			/*if(player1Score >= player2Score && singleton.onePlayer)
				singleton.wins++;
			else if(player1Score < player2Score && singleton.onePlayer)
				singleton.losses++;*/
				
			var gameOverScreen = new GameOverScreen();
			gameOverScreen.width = Constants.SCREEN_WIDTH;
			gameOverScreen.height = Constants.SCREEN_HEIGHT;
			addChild(gameOverScreen);
			scorePane.x = (Constants.SCREEN_WIDTH - scorePane.width)/2;
			scorePane.y = (Constants.SCREEN_HEIGHT - scorePane.height)/2;
			setChildIndex(scorePane, this.numChildren - 1);
			geometricAssets.push(gameOverScreen);
			setChildIndex(bButton, this.numChildren-1);
		}
		
		public function refreshDotColors(dotNode1:Dot, dotNode2:Dot = null)
		{
			/* dots array has size equals to 2, selected dots. */
			dotNode1.dot.gotoAndStop(1);
			if(dotNode2 != null)
				dotNode2.dot.gotoAndStop(1);
		}
		public function goBackToMenu(e:Event)
		{
			singleton.soundController.playClickSound();
			this.removeEventListener(MouseEvent.MOUSE_DOWN, onClickStage);
			e.currentTarget.removeEventListener(MouseEvent.MOUSE_DOWN, goBackToMenu);
			var confirmationScreen = setupConfirmationScreen();
			addChild(confirmationScreen);
		}
		private function setupConfirmationScreen()
		{
			available = false;
			var confirmationScreen = new BackConfirmation();
			confirmationScreen.bg.width = Constants.SCREEN_WIDTH;
			confirmationScreen.bg.height = Constants.SCREEN_HEIGHT;
			confirmationScreen.screen.width = Constants.SCREEN_WIDTH;
			confirmationScreen.screen.scaleY = confirmationScreen.screen.scaleX;
			confirmationScreen.screen.x = (Constants.SCREEN_WIDTH - confirmationScreen.screen.width)/2;
			confirmationScreen.screen.y = (Constants.SCREEN_HEIGHT - confirmationScreen.screen.height)/2;
			confirmationScreen.screen.yes.addEventListener(MouseEvent.MOUSE_DOWN, yes);
			confirmationScreen.screen.no.addEventListener(MouseEvent.MOUSE_DOWN, no);
			return confirmationScreen;
			
		}
		public function yes(e:Event)
		{
			singleton.soundController.playClickSound();
			e.currentTarget.removeEventListener(MouseEvent.MOUSE_DOWN, yes);
			for(var i:int = 0; i < geometricAssets.length; i++)
				removeChild(geometricAssets[i]);
			for(var p:int = 0; p < fakeEdges.length; p++)
			{
				removeChild(fakeEdges[p]);
				fakeEdges[p].removeEventListener(MouseEvent.MOUSE_DOWN, forceConnect);
			}
			for(var k:int = 0; k < dots.length; k++)
			{
				for(var j:int = 0; j < dots[k].length; j++)
				{
					dots[k][j].removeChildrens();
					/*dots[k][j].removeEventListener(MouseEvent.MOUSE_DOWN, onClickDot);*/
				}
			}
			bButton.removeEventListener(MouseEvent.MOUSE_DOWN, goBackToMenu);
			dispatchEvent(new Event(Constants.GO_BACK_MENU_EVENT));
			singleton.soundController.silence();
			singleton.soundController.playMenuSound();
		}
		public function no(e:Event)
		{
			singleton.soundController.playClickSound();
			this.addEventListener(MouseEvent.MOUSE_DOWN, onClickStage);
			bButton.addEventListener(MouseEvent.MOUSE_DOWN, goBackToMenu);
			e.currentTarget.removeEventListener(MouseEvent.MOUSE_DOWN, no);
			removeChild(e.currentTarget.parent.parent);
			available = true;
			playerTurn.canMove();
		}
		private function inchesToPixels(inches:Number):uint
		{
		   return Math.round(ppi * inches);
		}
	}
}
