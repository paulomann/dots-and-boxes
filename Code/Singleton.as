﻿package  
{	
	public class Singleton 
	{
		private static var singleton:Singleton;
		public var AIDifficulty:int;
		public var AIGame:Boolean;
		public var twoPlayer:Boolean;
		public var onePlayer:Boolean;
		public var squaresClosed:int;
		public var wins:int;
		public var losses:int;
		public var totalGamesPlayed:int;
		public var loggedOnFacebook:Boolean;
		public var sharedOnSocialNetwork:Boolean;
		public var soundController:SoundController;
		public var playerSelecting:int;
		
		public function Singleton()
		{
			if(singleton != null)
			{
				throw new Error("Singleton can only be accessed through Singleton.getInstance");
			}
		}
		public static function getInstance():Singleton
		{
			if(!singleton)
			{
				singleton = new Singleton();
			}
			return singleton;
		}
	}
}
