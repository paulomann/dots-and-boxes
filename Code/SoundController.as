﻿package  
{	
	import flash.media.*;
	import flash.media.SoundMixer;
	public class SoundController 
	{
		private var mute;
		private var gameSound;
		private var gameSoundChannel;
		
		private var menuSound;
		private var menuSoundChannel;
		
		private var fenceSound;
		private var fenceSoundChannel;
		
		private var pigSound;
		private var pigSoundChannel;
		
		private var clickSound;
		private var clickSoundChannel;
		
		private var manSound;
		private var manSoundChannel;
		
		public function SoundController() 
		{
			mute = false;
			gameSound = new GameSound();
			menuSound = new MenuSound();
			fenceSound = new FenceSound();
			pigSound = new PigSound();
			clickSound = new ClickSound();
			manSound = new ManSound();
		}
		public function playMenuSound()
		{
			var volume = mute ? 0 : 0.20;
			var sTransform = new SoundTransform(volume);
			menuSoundChannel = menuSound.play(0,1000, sTransform);
		}
		public function playGameSound()
		{
			var volume = mute ? 0 : 0.25;
			var sTransform = new SoundTransform(volume);
			gameSoundChannel = gameSound.play(0,1000, sTransform);
		}
		public function playFenceSound()
		{
			var volume = mute ? 0 : 1;
			var sTransform = new SoundTransform(volume);
			fenceSoundChannel = fenceSound.play(0,0, sTransform);
		}
		public function playPigSound()
		{
			var volume = mute ? 0 : 0.15;
			var sTransform = new SoundTransform(volume);
			pigSoundChannel = pigSound.play(0,0, sTransform);
		}
		public function playClickSound()
		{
			var volume = mute ? 0 : 0.20;
			var sTransform = new SoundTransform(volume);
			clickSoundChannel = clickSound.play(0,0, sTransform);
		}
		public function playManSound()
		{
			var volume = mute ? 0 : 0.20;
			var sTransform = new SoundTransform(volume);
			manSoundChannel = manSound.play(0,0, sTransform);
		}
		public function silence() // ultimate do silence
		{
			var sTransform = new SoundTransform(0);
			if(gameSoundChannel)
			{
				gameSoundChannel.soundTransform = sTransform;
				gameSoundChannel.stop();
			}
			if(menuSoundChannel)
			{
				menuSoundChannel.soundTransform = sTransform;
				menuSoundChannel.stop();
			}
			if(fenceSoundChannel)
			{
				fenceSoundChannel.soundTransform = sTransform;
				fenceSoundChannel.stop();
			}
			if(pigSoundChannel)
			{
				pigSoundChannel.soundTransform = sTransform;
				pigSoundChannel.stop();
			}
			if(manSoundChannel)
			{
				manSoundChannel.soundTransform = sTransform;
				manSoundChannel.stop();
			}
		}
		public function stopSound()
		{
			var sTransform = new SoundTransform(0);
			if(gameSoundChannel)
				gameSoundChannel.soundTransform = sTransform;
			if(menuSoundChannel)
				menuSoundChannel.soundTransform = sTransform;
			if(fenceSoundChannel)
				fenceSoundChannel.soundTransform = sTransform;
			if(pigSoundChannel)
				pigSoundChannel.soundTransform = sTransform;
			if(manSoundChannel)
				manSoundChannel.soundTransform = sTransform;
			mute = true;
		}
		public function restoreSound()
		{
			if(gameSoundChannel)
				gameSoundChannel.soundTransform = new SoundTransform(0.25);
			if(menuSoundChannel)
				menuSoundChannel.soundTransform = new SoundTransform(0.20);
			if(fenceSoundChannel)
				fenceSoundChannel.soundTransform = new SoundTransform(1);
			if(pigSoundChannel)
				pigSoundChannel.soundTransform = new SoundTransform(0.15);
			if(manSoundChannel)
				manSoundChannel.soundTransform = new SoundTransform(0.2);
			mute = false;
		}
	}
}
